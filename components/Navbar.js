import Link from 'next/Link';
import { useSelector } from 'react-redux';
import style from '../styles/Navbar.module.css';

function Navbar() {
    const save = useSelector(state => state.save)
    return (
        <>
            <div className={style.nav}>
                <div className={style.navLogo}>Id.news</div>
                <ul>
                    <li>
                        <Link href='/'>
                            <a>Home</a>
                        </Link>
                    </li>
                    <li>
                        <Link href='/saved'>
                            <a>Saved</a> 
                        </Link>
                        <span className={style.counter}>{save}</span>
                    </li>
                </ul>
            </div>
        </>
    )
}

export default Navbar;
