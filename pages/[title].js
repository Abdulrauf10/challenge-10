import Navbar from "../components/Navbar";
import styles from '../styles/Home.module.css';
import axios from "axios";

function read({title, contentId}) {
    const content = contentId[0].content;
    return (
        <>
        <Navbar/>
        <div className={styles.container}>
            <h2>{title}</h2>
            <h6 className={styles.card}>{content}</h6>
        </div>
        </>
    )
}

export const getStaticProps = async (contex) => {
    const title = contex.params.title;
    const content = await axios.get("https://newsapi.org/v2/everything?q=tesla&from=2021-08-03&sortBy=publishedAt&apiKey=e5d80497fc9d497f80284880c4d1a795");
    const {data} = content
    return {
        props: {
            title,
            contentId: data.articles
        }
    }
}

export const getStaticPaths = async () => {
    const news = await axios.get("https://newsapi.org/v2/everything?q=tesla&from=2021-08-03&sortBy=publishedAt&apiKey=e5d80497fc9d497f80284880c4d1a795");
    const {data} = news;
    const paths = data.articles.map((news) => ({
        params: {
            title: news.title,
        }
    }))

    return {paths, fallback: false}
}

export default read;
