import {Provider} from 'react-redux';
import { createStore } from 'redux';
import allReducers from '../redux/reducers';
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  const store = createStore(allReducers);

  return (
  <Provider store={store}>
    <Component {...pageProps} />
  </Provider>)
}

export default MyApp
