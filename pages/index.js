import styles from '../styles/Home.module.css';
import axios from 'axios';
import Link from 'next/link';
import {useSelector, useDispatch} from 'react-redux';
import {saveAction} from '../redux/actions';
import Navbar from '../components/Navbar';

export default function Home({ data }) {
   const save = useSelector(state => state.save);
   const dispatch = useDispatch();
  return (
    <>
    <Navbar/> 
    <div className={styles.container}>
       {data.map((item, index) => {
         return (
            <div className={styles.card} key={index}>
              <h2>{item.title}</h2>
              <p>{item.description}</p>
              <Link href={`${item.title}`} key={index}>
              <button className={styles.button}>Read more</button>
              </Link>
                <button className={styles.button} onClick={() => dispatch(saveAction())}>Save</button>
              <p className={styles.date}>{item.publishedAt}</p>
            </div>
         )
       })}
    </div>
    </>
  )
}

export const getStaticProps = async () => {
  const news = await axios.get("https://newsapi.org/v2/everything?q=tesla&from=2021-08-03&sortBy=publishedAt&apiKey=e5d80497fc9d497f80284880c4d1a795");
  const {data} = news;
  return {
    props: {
      data: data.articles,
    }
  }
};