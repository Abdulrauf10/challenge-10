import {combineReducers} from 'redux';
import SaveReducer from './SaveReducer';

const allReducers = combineReducers({
    save: SaveReducer
});

export default allReducers;